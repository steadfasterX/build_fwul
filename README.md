## Build environment/scripts for FWUL 
### ([F]orget [W]indows [U]se [L]inux)

Official XDA thread https://bit.do/FWULatXDA

## Setup & Prepare (Arch Linux)

No other distribution then Arch Linux is supported (for both: build system and FWUL release).

1. `pacman -S archiso jq pacman-contrib`
1. `git clone https://code.binbash.it:8443/Carbon-Fusion/build_fwul.git ~/build_fwul`


## Usage / Build

### 64 bit (only available arch)

This is the only supported architecture for FWUL! 

1. `cd ~/build_fwul`
1. `sudo ./build_x64.sh -A x86_64 -F`

Use `./build_x64.sh --help` to find all possible options like working directory etc.


## Rebuild / Update ISO

1. `cd ~/build_fwul`
1. Add the option "-C"  or "-F" option to the instruction in "Usage / Build"
