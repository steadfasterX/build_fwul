#!/bin/bash
####################################################################################
#
# Workaround script for:
# https://code.binbash.it:8443/FWUL/build_fwul/issues/88
#
# Checks for the set language and offers a logout including a lightdm restart!
#
####################################################################################

LANGFILE="REPLACEHOME/.dmrc"
DMLANG=$(cat "$LANGFILE" | grep ^Language= | cut -d '=' -f 2 | tr '[:upper:]' '[:lower:]')
WLANG=$(echo "$LANG" | tr '[:upper:]' '[:lower:]')

if [ "$WLANG" != "$DMLANG" ];then
    yad --title="Set FWUL language" --fixed --height=200 --width=600 --image="REPLACEHOME/.fwul/warning_64x64.png"  --text "\n   It seems you have changed the desktop language ($LANG vs $DMLANG).\n\n   To fully apply the change you need to login again by clicking the\n\n   <b>\"Apply &amp; Logout\"</b>\n\n   button below\n\n   (this is a workaround and will be removed in a future release)" --button="Apply &amp; Logout":2
        sudo systemctl restart lightdm
fi
