#!/bin/bash

sudo pacman --noconfirm -Sy
sudo trizen --noconfirm -Sy

pacman -Q xperia-flashtool
if [ $? -ne 0 ];then
    $HOME/.fwul/install_package.sh AUR xperia-flashtool
    ### upstream outdated! issue #70 workaround
    #cd /tmp/
    #wget http://leech.binbash.it:8008/misc/xperia-flashtool-interim.pkg.tar.xz -O xperia-flashtool-interim.pkg.tar.xz
    #pacman -Q libselinux || sudo $HOME/.fwul/install_package.sh AUR libselinux
    #sudo pacman --noconfirm -U xperia-flashtool-interim.pkg.tar.xz 
    #rm xperia-flashtool-interim.pkg.tar.xz
    ### upstream outdated! issue #70 workaround
fi

pacman -Q libselinux && pacman -Q xperia-flashtool && cp ~/.fwul/sonyflash.desktop ~/Desktop/ && chmod +x ~/Desktop/sonyflash.desktop && rm ~/Desktop/install-sonyflash.desktop

