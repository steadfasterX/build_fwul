#!/bin/bash
###########################################################################################
#
# X and terminal session starter for FWUL
#
###########################################################################################

######
# Language & Locale
LANGFILE="REPLACEHOME/.dmrc"

if [ -f "$LANGFILE" ];then
    # extract, fix and export $LANG
    TEMPLANG=$(cat "$LANGFILE" | grep ^Language= | cut -d '=' -f 2 | sed 's/utf8/UTF8/')
    [ ! -z "$TEMPLANG" ] && export LANG=$TEMPLANG

    # convert $LANG to $LANGUAGE and export
    SETLAYOUT="$(echo $LANG | cut -d '@' -f 1 | cut -d '.' -f 1 | cut -d '_' -f 1)"

    setxkbmap -layout $SETLAYOUT

    # set global system locale
    DMLANG=$(cat "$LANGFILE" | grep ^Language= | cut -d '=' -f 2 | sed 's/utf8/UTF-8/')
    echo "LANG=\"$DMLANG\"" > /tmp/locale.conf
    # set fallback language
    echo "LANGUAGE=\"en_US.UTF-8\"" >> /tmp/locale.conf
    sudo mv /tmp/locale.conf /etc/locale.conf

    # ensure PAM locale is set, see issue #88. 
    # This also fixes an issue where the language does not change in the greeter top right after logging out
    grep -v "LANG=" /etc/environment > /tmp/environment
    echo "LANG=\"$DMLANG\"" >> /tmp/environment
    sudo mv /tmp/environment /etc/environment
fi

# detect FWUL mode
grep -v "fwul_" /etc/fwul-release > /tmp/fwul-release
hostnamectl status |grep -v ID |tr -d " " | tr ":" "=" |sed 's/^/fwul_/g' >> /tmp/fwul-release
grep fwulversion /tmp/fwul-release >> /dev/null && sudo mv /tmp/fwul-release /etc/fwul-release

