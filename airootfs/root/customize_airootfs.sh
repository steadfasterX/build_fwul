#!/bin/bash
#######################################################################################################

set -e -u

# debug means e.g. SSH server will be added and enabled
# 1 means debug on, 0 off.
DEBUG=0

ntpdate de.pool.ntp.org 

locale-gen

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /bin/bash root
cp -aT /etc/skel/ /root/
chmod 700 /root

sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

# some vars
TMPSUDOERS=/etc/sudoers.d/build
RSUDOERS=/etc/sudoers.d/fwul
LOGINUSR=android
LOGINPW=linux
RPW=$LOGINPW
TINYCLONE="git clone --depth 1 --no-single-branch"

# set fake release info (get overwritten later)
echo "fwulversion=0.0" > /etc/fwul-release
echo "fwulbuild=0" >> /etc/fwul-release
echo "patchlevel=0" >> /etc/fwul-release

# current java version provided with FWUL (to save disk space compressed and not installed)
CURJAVA="jre-8u131-1-${arch}.pkg.tar.xz"
INSTJAVA="sudo pacman -U --noconfirm /home/$LOGINUSR/.fwul/jre-8u131-1-${arch}.pkg.tar.xz"

# add live user but ensure this happens when not there already
echo -e "\nuser setup:"
! id $LOGINUSR && useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /bin/bash $LOGINUSR
id $LOGINUSR
passwd $LOGINUSR <<EOSETPW
$LOGINPW
$LOGINPW
EOSETPW

# prepare user home
cp -avT /etc/fwul/ /home/$LOGINUSR/
[ ! -d /home/$LOGINUSR/Desktop ] && mkdir /home/$LOGINUSR/Desktop
chmod 700 /home/$LOGINUSR
chown -R $LOGINUSR /home/$LOGINUSR 

# prepare hosts
cp /etc/hosts /etc/hosts.orig
echo "10.0.228.17     leech.binbash.it" >> /etc/hosts

# add user to required groups
usermod -a -G vboxsf $LOGINUSR 

# session hacks
sed -i "s#REPLACEVBOXHOME#/home/$LOGINUSR/Desktop#g" /etc/systemd/scripts/fwul-session.sh
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /etc/profile.d/fwul-session.sh
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /home/$LOGINUSR/.fwul/checklang.sh

# temp perms for archiso
[ -f $RSUDOERS ]&& rm -vf $RSUDOERS      # ensures an update build will not fail
cat > $TMPSUDOERS <<EOSUDOERS
ALL     ALL=(ALL) NOPASSWD: ALL
EOSUDOERS

# add the pacman ignore list
# https://code.binbash.it:8443/FWUL/build_fwul/issues/63
sed -i '/\[options\]/a Include=/etc/pacman.ignore' /etc/pacman.conf
cat > /etc/pacman.ignore<<EOPIGN
# PARTIAL UPGRADES ARE NOT SUPPORTED! USE THIS WITH CARE!
# Check this first:
# https://wiki.archlinux.org/index.php/System_maintenance#Partial_upgrades_are_unsupported

# The option IgnorePkg EXTENDS the pacman ignore list.
# Format: ONE package for every ignore line! Do *not* use space delimitated syntax here!

# uefi and syslinux
IgnorePkg = efibootmgr
IgnorePkg = efitools
IgnorePkg = efivar
IgnorePkg = refind-efi
IgnorePkg = syslinux

# kernel, kernel headers and ramdisk tools
IgnorePkg = mkinitcpio-busybox
IgnorePkg = mkinitcpio
IgnorePkg = linux*

# virtualbox module
IgnorePkg = virtualbox-guest-utils

# extreme CPU intensive and usually not needed to upgrade
IgnorePkg = qtwebkit
EOPIGN

if [ $arch == "x86_64" ];then
    # init pacman + multilib
    # O M G ! This is so crappy bullshit! when build.sh see's 
    # the returncode 1 it just stops?!! so I use that funny workaround..
    RET=$(egrep -q '^\[multilib' /etc/pacman.conf||echo missing)
    if [ "$RET" == "missing" ];then
        echo "adding multilib to conf"
        cat >>/etc/pacman.conf<<EOPACMAN
[multilib]
SigLevel = PackageRequired
Include = /etc/pacman.d/mirrorlist
EOPACMAN
    else
        echo skipping multilib because it is configured already
    fi
else
    echo "SKIPPING multilib because of $arch"
fi

# adding antergos and arch32 mirrors
#RET=$(egrep -q '^\[antergos' /etc/pacman.conf||echo missing)
#if [ "$RET" == "missing" ];then
#    echo "adding custom mirrors to conf"
#    cat >>/etc/pacman.conf<<EOPACMAN
#[antergos]
#Include = /etc/pacman.d/fwul-mirrorlist
#EOPACMAN
#else
#    echo skipping antergos mirror because it is configured already
#fi

# disable free disk check
sed -i 's/^CheckSpace/#CheckSpace/' /etc/pacman.conf

# initialize the needed keyrings (again)
haveged -w 1024
pacman-key --init
pacman-key --populate archlinux manjaro
pacman-key --refresh-keys
su -c - $LOGINUSR "trizen -Syyu --noconfirm" || echo "update ended with $?" 

# workaround until systemd is =>233
#pacman -Syu --noconfirm --ignore netctl

# install yad
echo -e "\nyad:"
trizen -Q yad || su -c - $LOGINUSR "trizen -S --noconfirm yad"

# install python zstandard for newer extracting lg kdz
trizen -Q python-zstandard || su -c - $LOGINUSR "trizen -S --noconfirm python-zstandard"

# workaround until fixed upstream (only activated when AUR package isn't fixed)
# https://aur.archlinux.org/packages/python-zstandard/#comment-674955
if [ ! -d /usr/lib/python3.7/site-packages/zstandard ];then
    $TINYCLONE https://aur.archlinux.org/python-zstandard.git /tmp/zstd 
    cd /tmp/zstd
    su -c - $LOGINUSR "makepkg"
    cp -av src/zstandard-*/zstandard /usr/lib/python3.7/site-packages/
    echo "WORKAROUND ADDED FOR PYTHON-ZSTANDARD!"
    rm -rf /tmp/zstd
fi

# GUI based package manager
cp /usr/share/applications/pamac-manager.desktop /home/$LOGINUSR/Desktop/
sed -i 's/Icon=.*/Icon=gnome-software/g' /home/$LOGINUSR/Desktop/pamac-manager.desktop

# disable tray to avoid bothering users for updating
[ -f /etc/xdg/autostart/pamac-tray.desktop ] && rm /etc/xdg/autostart/pamac-tray.desktop

# prepare Samsung tool dir
[ ! -d /home/$LOGINUSR/Desktop/Samsung ] && mkdir /home/$LOGINUSR/Desktop/Samsung

# install udev-rules
[ ! -d /home/$LOGINUSR/.android ] && mkdir /home/$LOGINUSR/.android
[ ! -f /home/$LOGINUSR/.android/adb_usb.ini ] && wget https://github.com/M0Rf30/android-udev-rules/raw/704f83d8528a56c969dab8a9b63bf83279d4c71b/adb_usb.ini -O /home/$LOGINUSR/.android/adb_usb.ini

# always update the udev rules to be top current
wget https://raw.githubusercontent.com/M0Rf30/android-udev-rules/master/51-android.rules -O /etc/udev/rules.d/51-android.rules

# install & prepare tmate
echo -e "\ntmate:"
trizen -Q tmate || su -c - $LOGINUSR "trizen -S --noconfirm tmate"
# install the ssh keygenerator
[ ! -d /home/$LOGINUSR/.config/autostart ] && mkdir -p /home/$LOGINUSR/.config/autostart && chown -R $LOGINUSR /home/$LOGINUSR/.config/autostart
echo -e '#!/bin/bash\n[ !'" -f /home/$LOGINUSR/.ssh/id_rsa ] && ssh-keygen -a 100 -t ed25519 -f /home/$LOGINUSR/.ssh/id_ed25519 -P ''" > /home/$LOGINUSR/.fwul/sshkeygen.sh
cat > /home/$LOGINUSR/.config/autostart/sshkeygen.desktop<<EOSSHKG
[Desktop Entry]
Version=1.0
Type=Application
Comment=sshkeygen
Terminal=false
Name=sshkeygen
Exec=/home/$LOGINUSR/.fwul/sshkeygen.sh
EOSSHKG
chmod +x /home/$LOGINUSR/.fwul/sshkeygen.sh

# hexchat config: ensure username replacement on every boot (unless persistent install)
echo -e '#!/bin/bash\nif [ !'" -f /home/$LOGINUSR/.config/hexchat/.configured ];then GEN=\$(pwgen -BA01); sed -i s#fwul_REPLACE#fwul_\${GEN}#g /home/$LOGINUSR/.config/hexchat/hexchat.conf && > /home/$LOGINUSR/.config/hexchat/.configured; fi" > /home/$LOGINUSR/.fwul/hex.sh
cat > /home/$LOGINUSR/.config/autostart/hex.desktop<<EOSSHKG
[Desktop Entry]
Version=1.0
Type=Application
Comment=autostarted on login
Terminal=false
Name=hexchat configuration
Exec=/home/$LOGINUSR/.fwul/hex.sh
EOSSHKG
chmod +x /home/$LOGINUSR/.fwul/hex.sh

cat > /home/$LOGINUSR/Desktop/io.github.Hexchat.desktop <<EODH
[Desktop Entry]
Version=1.0
Type=Application
Comment=IRC client
Terminal=false
Name=IRC (hexchat)
Icon=hexchat
Categories=GTK;Network;IRCClient;
X-GNOME-UsesNotifications=true
Keywords=IM;Chat;
Exec=hexchat --existing %U
EODH
chown $LOGINUSR /home/$LOGINUSR/Desktop/io.github.Hexchat.desktop
chmod +x /home/$LOGINUSR/Desktop/io.github.Hexchat.desktop

# install & add Heimdall
echo -e "\nheimdall:"

trizen -Q heimdall-git || su -c - $LOGINUSR "trizen -S --noconfirm heimdall-git"
# workaround (issue #71) as heimdall moved to gitlab but AUR is outdated:
#wget -O /tmp/hd.pkg.tar.xz "http://leech.binbash.it:8008/misc/heimdall-git-1.4.2.r5.g5377b62-1-x86_64.pkg.tar.xz"
#pacman -U --noconfirm /tmp/hd.pkg.tar.xz && echo "heimdall-git: workaround for issue #71 applied successfully"
#rm /tmp/hd.pkg.tar.xz

cp /usr/share/applications/heimdall.desktop /home/$LOGINUSR/Desktop/Samsung/
# fix missing heimdall icon
sed -i 's/Icon=.*/Icon=heimdall-frontend/g' /home/$LOGINUSR/Desktop/Samsung/heimdall.desktop

# install testdisk/photorec incl. GUI support
# (https://code.binbash.it:8443/FWUL/build_fwul/issues/66)
echo -e "\nphotorec:"
trizen -Q testdisk && pacman --noconfirm -R testdisk
trizen -Q qt5-tools || pacman --noconfirm -S qt5-tools
trizen -Q testdisk-wip || su -c - $LOGINUSR "trizen -S --noconfirm testdisk-wip"
cp -v /usr/share/applications/qphotorec.desktop /home/$LOGINUSR/Desktop/
chmod +x /home/$LOGINUSR/Desktop/qphotorec.desktop 
chown $LOGINUSR /home/$LOGINUSR/Desktop/qphotorec.desktop

# install welcome screen
echo -e "\nwelcome-screen:"
if [ ! -d /home/$LOGINUSR/programs/welcome ];then
    $TINYCLONE https://code.binbash.it:8443/FWUL/fwul_welcome.git /home/$LOGINUSR/programs/welcome
    # install the regular welcome screen
    if [ ! -f /home/$LOGINUSR/.config/autostart/welcome.desktop ];then
        [ ! -d /home/$LOGINUSR/.config/autostart ] && mkdir -p /home/$LOGINUSR/.config/autostart && chown -R $LOGINUSR /home/$LOGINUSR/.config/autostart
        cat > /home/$LOGINUSR/.config/autostart/welcome.desktop<<EOWAS
[Desktop Entry]
Version=1.0
Type=Application
Comment=FWUL Welcome Screen
Terminal=false
Name=Welcome
Exec=/home/$LOGINUSR/programs/welcome/welcome.sh
Icon=/home/$LOGINUSR/programs/welcome/icons/welcome.png
EOWAS
    fi
    # install the force welcome screen (when user manually want to start it)
    if [ ! -f /home/$LOGINUSR/Desktop/welcome.desktop ];then
        cat > /home/$LOGINUSR/Desktop/welcome.desktop <<EOWASF
[Desktop Entry]
Version=1.0
Type=Application
Comment=FWUL Welcome Screen
Terminal=false
Name=Welcome
Exec=/home/$LOGINUSR/programs/welcome/welcome-force.sh
Icon=/home/$LOGINUSR/programs/welcome/icons/welcome.png
EOWASF
    fi
    chmod +x /home/$LOGINUSR/.config/autostart/welcome.desktop /home/$LOGINUSR/Desktop/welcome.desktop
fi

# install workaround script for language bug
# https://code.binbash.it:8443/FWUL/build_fwul/issues/88
[ ! -d /home/$LOGINUSR/.config/autostart ] && mkdir -p /home/$LOGINUSR/.config/autostart && chown -R $LOGINUSR /home/$LOGINUSR/.config/autostart 
cat > /home/$LOGINUSR/.config/autostart/language.desktop<<EOLNGBUG
[Desktop Entry]
Version=1.0
Type=Application
Comment=autostarted on login
Terminal=false
Name=language workaround
Exec=/home/android/.fwul/checklang.sh
EOLNGBUG
chmod +x /home/$LOGINUSR/.fwul/checklang.sh

# install JOdin3
if [ $arch == "x86_64" ];then
    if [ ! -d /home/$LOGINUSR/programs/JOdin ];then
        mkdir /home/$LOGINUSR/programs/JOdin
        cat >/home/$LOGINUSR/programs/JOdin/starter.sh <<EOEXECOD
#!/bin/bash
trizen -Q jre || xterm -e "$INSTJAVA"
JAVA_HOME=/usr/lib/jvm/java-8-jre /home/$LOGINUSR/programs/JOdin/JOdin3CASUAL
EOEXECOD
        chmod +x /home/$LOGINUSR/programs/JOdin/starter.sh
        # wget "http://droid.binbash.it:8008/misc/JOdin3CASUAL-r1142-dist.tar.gz" -O JOdin.tgz
        wget "http://localhost:8008/misc/JOdin3CASUAL-r1142-dist.tar.gz" -O JOdin.tgz
        tar -xzf JOdin.tgz -C /home/$LOGINUSR/programs/JOdin/ && rm -rf /home/$LOGINUSR/programs/JOdin/runtime JOdin.tgz
        cat >/home/$LOGINUSR/Desktop/Samsung/JOdin.desktop <<EOODIN
[Desktop Entry]
Version=1.0
Type=Application
Comment=Odin for Linux
Terminal=false
Name=JOdin3
Exec=/home/$LOGINUSR/programs/JOdin/starter.sh
Icon=/home/$LOGINUSR/.fwul/odin-logo.jpg
EOODIN
        chmod +x /home/$LOGINUSR/Desktop/Samsung/JOdin.desktop
    fi
else
    echo "SKIPPING JODIN INSTALL: Arch $arch detected!"
fi

# chromium installer
cat >/home/$LOGINUSR/Desktop/install-chromium.desktop <<EOsflashinst
[Desktop Entry]
Version=1.0
Type=Application
Comment=Chromium Browser Installer
Terminal=false
Name=Chromium installer
Exec=/home/$LOGINUSR/.fwul/install_chromium.sh
Icon=aptoncd
EOsflashinst
chmod +x /home/$LOGINUSR/Desktop/install-chromium.desktop

cat >/home/$LOGINUSR/.fwul/sonyflash.desktop <<EOsflash
[Desktop Entry]
Version=1.0
Type=Application
Comment=Sony FlashTool
Terminal=false
Name=Sony Flashtool
Exec=xperia-flashtool
Icon=/home/$LOGINUSR/.fwul/flashtool-icon.png
EOsflash

# teamviewer installer
echo -e "\nteamviewer:"
cat >/home/$LOGINUSR/Desktop/install-TV.desktop <<EOODIN
[Desktop Entry]
Version=1.0
Type=Application
Comment=Teamviewer installer
Terminal=true
Name=TeamViewer Installer
Exec=pkexec /home/$LOGINUSR/.fwul/install_tv.sh
Icon=aptoncd
EOODIN
chmod +x /home/$LOGINUSR/Desktop/install-TV.desktop

# even when there is an old/outdated version of spflashtool on an old website it is not possible to 
# get it working on 32bit
# http://spflashtool.org/download/SP_Flash_Tool_Linux_32Bit_v5.1520.00.100.zip <--- THIS CONTAINS 64bit BINARIES!!!
if [ $arch == "x86_64" ];then
    # SP Flash Tools installer
    echo -e "\nSP Flash Tools:"
    cat >/home/$LOGINUSR/Desktop/install-spflash.desktop <<EOSPF
[Desktop Entry]
Version=1.0
Type=Application
Comment=SP FlashTools installer
Terminal=false
Name=SP FlashTools Installer
Exec=/home/$LOGINUSR/.fwul/install_spflash.sh
Icon=aptoncd
EOSPF
    chmod +x /home/$LOGINUSR/Desktop/install-spflash.desktop
fi

# Sony Flash Tools installer
echo -e "\nSony Flash Tools:"
cat >/home/$LOGINUSR/Desktop/install-sonyflash.desktop <<EOSFT
[Desktop Entry]
Version=1.0
Type=Application
Comment=Sony FlashTools installer
Terminal=true
Name=Sony FlashTools Installer
Exec=/home/$LOGINUSR/.fwul/install_sonyflash.sh
Icon=aptoncd
EOSFT
chmod +x /home/$LOGINUSR/Desktop/install-sonyflash.desktop

# prepare LG tools
[ ! -d /home/$LOGINUSR/Desktop/LG ] && mkdir /home/$LOGINUSR/Desktop/LG/

# SALT
echo -e "\nSALT:"
[ ! -d /home/$LOGINUSR/programs/SALT ] && $TINYCLONE https://github.com/steadfasterX/salt.git /home/$LOGINUSR/programs/SALT
[ ! -d /root/programs/lglafng ] && $TINYCLONE https://github.com/steadfasterX/lglaf.git /home/$LOGINUSR/programs/lglafng
[ ! -d /root/programs/kdztools ] && $TINYCLONE https://github.com/steadfasterX/kdztools.git /home/$LOGINUSR/programs/kdztools
[ ! -d /root/programs/sdat2img ] && $TINYCLONE https://github.com/xpirt/sdat2img.git /home/$LOGINUSR/programs/sdat2img
if [ ! -f /home/$LOGINUSR/Desktop/LG/SALT.desktop ];then
    cat > /home/$LOGINUSR/Desktop/LG/SALT.desktop <<EOFDSK
[Desktop Entry]
Version=1.0
Type=Application
Terminal=false
Name=SALT
Icon=/home/$LOGINUSR/programs/SALT/icons/salt_icon.png
Comment=SALT - [S]teadfasterX [A]ll-in-one [L]G [T]ool
Exec=pkexec /home/$LOGINUSR/programs/SALT/salt
EOFDSK
fi
if [ ! -f /home/$LOGINUSR/Desktop/LG/SALT_fb.desktop ];then
    cat > /home/$LOGINUSR/Desktop/LG/SALT_fb.desktop <<EOFDSKFB
[Desktop Entry]
Version=1.0
Type=Application
Terminal=true
Name=SALT (debug)
Icon=/home/$LOGINUSR/programs/SALT/icons/salt_icon.png
Comment=SALT - [S]teadfasterX [A]ll-in-one [L]G [T]ool
Exec=pkexec /home/$LOGINUSR/programs/SALT/salt
EOFDSKFB
fi
chmod +x /home/$LOGINUSR/Desktop/LG/*.desktop

# pure LG LAF
echo -e "\npure lglaf:"
[ ! -d /home/$LOGINUSR/programs/lglaf ] && $TINYCLONE https://github.com/Lekensteyn/lglaf.git /home/$LOGINUSR/programs/lglaf

# LG LAF shortcut with auth
echo -e "\nLG LAF shortcut with auth:"
cat >/home/$LOGINUSR/Desktop/LG/open-lglafshell.desktop <<EOSFT
[Desktop Entry]
Version=1.0
Type=Application
Comment=LG LAF
Terminal=false
Name=LG LAF (PeterWu)
Exec=xfce4-terminal --working-directory=/home/$LOGINUSR/programs/lglaf
Icon=terminal
EOSFT
chmod +x /home/$LOGINUSR/Desktop/LG/open-lglafshell.desktop

# LGLAF (steadfasterX) 
echo -e "\nLG LAF NG shortcut:"
cat >/home/$LOGINUSR/Desktop/LG/open-lglafng.desktop <<EOLAFNG
[Desktop Entry]
Version=1.0
Type=Application
Comment=LG LAF with steadfasterX patches
Terminal=false
Name=LG LAF (steadfasterX)
Exec=xfce4-terminal --working-directory=/home/$LOGINUSR/programs/lglafng
Icon=terminal
EOLAFNG

# LGLAF (runningnak3d)
[ ! -d /home/$LOGINUSR/programs/lglafsploit ] && $TINYCLONE https://gitlab.com/runningnak3d/lglaf.git /home/$LOGINUSR/programs/lglafsploit
echo -e "\nLG LAF lafsploit shortcut:"
cat >/home/$LOGINUSR/Desktop/LG/open-lglafsploit.desktop <<EOLAFNG
[Desktop Entry]
Version=1.0
Type=Application
Comment=LG LAF with runningnak3d patches
Terminal=false
Name=LG LAF (runningnak3d)
Exec=xfce4-terminal --working-directory=/home/$LOGINUSR/programs/lglafsploit
Icon=terminal
EOLAFNG

# tmate
echo -e "\ntmate shortcut:"
cat >/home/$LOGINUSR/Desktop/tmate.desktop <<EOTMATE
[Desktop Entry]
Name=tmate - Simple Remote Control
GenericName=tmate
Comment=Terminal sharing with tmate
Exec=xfce4-terminal --maximize -e /home/$LOGINUSR/.fwul/tmate.sh
Icon=/home/$LOGINUSR/.fwul/tmate-logo.png
Terminal=false
Type=Application
EOTMATE
chmod +x /home/$LOGINUSR/.fwul/tmate.sh

# install display manager
echo -e "\nDM:"
#trizen -Q webkitgtk2 || su -c - $LOGINUSR "trizen -S --noconfirm webkitgtk2"
#trizen -Q mdm-display-manager || su -c - $LOGINUSR "trizen -S --noconfirm mdm-display-manager"
systemctl enable lightdm

# configure login/display manager
#cp -v /home/$LOGINUSR/.fwul/mdm.conf /etc/mdm/custom.conf
cp -v /home/$LOGINUSR/.fwul/lightdm-gtk-greeter.conf /etc/lightdm/
# copy background + icon for greeter
cp -v /home/$LOGINUSR/.fwul/fwul_login.png /usr/share/pixmaps/greeter_background.png
cp -v  /home/$LOGINUSR/.fwul/greeter_icon.png /usr/share/pixmaps/

# Special things needed for easier DEBUGGING
if [ "$DEBUG" -eq 1 ];then
    pacman -Q openssh || pacman -S --noconfirm openssh
    systemctl enable sshd
    pacman -Q spice-vdagent || pacman -S --noconfirm spice-vdagent
    systemctl enable spice-vdagentd
fi

# add simple ADB 
# (https://forum.xda-developers.com/android/software/revive-simple-adb-tool-t3417155, https://github.com/mhashim6/Simple-ADB)
cat >/home/$LOGINUSR/Desktop/ADB.desktop <<EOSADB
[Desktop Entry]
Version=1.0
Type=Application
Comment=adb and fastboot GUI
Terminal=false
Name=Simple-ADB
Exec=/home/$LOGINUSR/programs/sadb/starter.sh
Icon=/home/$LOGINUSR/programs/sadb/sadb.jpg
EOSADB

cat >/home/$LOGINUSR/programs/sadb/starter.sh <<EOEXECADB
#!/bin/bash
trizen -Q jre || xterm -e "$INSTJAVA"
java -jar /home/$LOGINUSR/programs/sadb/S-ADB.jar
EOEXECADB
chmod +x /home/$LOGINUSR/programs/sadb/starter.sh

# install android tools (see #8)
pacman -Q abootimg-git || su -c - $LOGINUSR "trizen -S --noconfirm abootimg-git" 
pacman -Q bootimgtool-git || su -c - $LOGINUSR "trizen -S --noconfirm bootimgtool-git" 

# install Xiaomi MiFlash
# https://code.binbash.it:8443/FWUL/build_fwul/issues/62
if [ ! -d /home/$LOGINUSR/programs/MiFlash ];then
    $TINYCLONE https://github.com/limitedeternity/MiFlash-Linux.git /home/$LOGINUSR/programs/MiFlash
    # ensure the stupid setup do not run in FWUL
    > /home/$LOGINUSR/.xiaomi_tool
    # fix help
    ln -s /home/$LOGINUSR/programs/MiFlash/README.md /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash/.bin/xiaomi_tools/README.txt
    sed -i 's/gedit/leafpad/g' /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash/.bin/xiaomi_tools/xiaomi_tools.cfg
    # catch the icon
    wget -O /home/$LOGINUSR/programs/MiFlash/Xiaomi.png https://www.xiaomiflash.com/img/Xiaomi.png
    # make it usable ( - do not use go.sh - )
    cat >/home/$LOGINUSR/programs/MiFlash/starter.sh <<EOEXECXIA
#!/bin/bash
cd /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash
xfce4-terminal --geometry=99x100 -x bash -c "\$PWD/.bin/xiaomi_tools/xiaomi_tools.cfg; bash"
EOEXECXIA
    chmod +x /home/$LOGINUSR/programs/MiFlash/starter.sh

    [ ! -d /home/$LOGINUSR/Desktop/Xiaomi/ ] && mkdir /home/$LOGINUSR/Desktop/Xiaomi  && chown $LOGINUSR /home/$LOGINUSR/Desktop/Xiaomi
    cat >/home/$LOGINUSR/Desktop/Xiaomi/miflash.desktop <<EODXIA
[Desktop Entry]
Version=1.0
Type=Application
Comment=
Terminal=false
Name=Xiaomi MiFlash
Exec=/home/$LOGINUSR/programs/MiFlash/starter.sh
Icon=/home/$LOGINUSR/programs/MiFlash/Xiaomi.png
Path=/home/$LOGINUSR/programs/MiFlash/
EODXIA
    # working dir shortcut
    echo -e "\nMiFlash working dir shortcut:"
    ln -s /home/$LOGINUSR/programs/MiFlash/ /home/$LOGINUSR/Desktop/Xiaomi/MiFlash_WorkDir
fi

# install payload extractor
[ ! -d /home/$LOGINUSR/programs/ROME ] && $TINYCLONE https://code.binbash.it:8443/FWUL/android_rome.git /home/$LOGINUSR/programs/ROME
echo -e "\nROME shortcut:"
cat >/home/$LOGINUSR/Desktop/rome.desktop <<EOXAPE
[Desktop Entry]
Version=1.0
Type=Application
Comment=ROME [ROM] [E]xtractor
Terminal=false
Name=ROME
Exec=/home/$LOGINUSR/programs/ROME/rome
Icon=/home/$LOGINUSR/programs/ROME/icons/rome_icon.png
EOXAPE
chmod +x /home/$LOGINUSR/Desktop/rome.desktop

# install FWUL LivePatcher
# https://code.binbash.it:8443/FWUL/build_fwul/issues/57
# $TINYCLONE https://code.binbash.it:8443/FWUL/arch_fwulpatch-pkg.git /tmp/fwulpatch \
#    && cd /tmp/fwulpatch \
#    && chown $LOGINUSR /tmp/fwulpatch \
#    && su -c - $LOGINUSR "makepkg -s" \
#    && pacman --noconfirm -U fwulpatch-*.pkg.tar.xz
#cd /

# makepkg is broken in chroot:
wget -O /tmp/fwulpatch.pkg.tar.xz "http://leech.binbash.it:8008/FWUL/.repo/fwulpatch.pkg.tar.xz"
pacman --noconfirm -U /tmp/fwulpatch.pkg.tar.xz
rm /tmp/fwulpatch.pkg.tar.xz

# make all desktop files usable
chmod +x /home/$LOGINUSR/Desktop/*.desktop /home/$LOGINUSR/Desktop/*/*.desktop
chown -R $LOGINUSR /home/$LOGINUSR/Desktop/

# enable services
systemctl enable pacman-init.service choose-mirror.service
systemctl set-default graphical.target
systemctl enable systemd-networkd
systemctl enable NetworkManager
systemctl enable init-mirror
systemctl enable vboxservice
systemctl enable fwul-session

# prepare theming stuff
echo -e "\nThemes:"
[ -d /usr/share/mdm/themes/Arc-Wise/ ] || tar -xvzf /home/$LOGINUSR/.fwul/tmp/login-theme.tgz -C /
# windows 10 icons URL in AUR are broken..
#trizen -Q windows10-icons || su -c - $LOGINUSR "trizen -S --noconfirm windows10-icons"
#trizen -Q windows10-icons || su -c - $LOGINUSR "trizen --noconfirm -Pi ~/.fwul/tmp/win10icons/"
#rm /home/$LOGINUSR/.fwul/tmp/win10icons/*.xz
# install numix circle instead (minimal)
trizen -Q numix-circle-icon-theme-git || su -c - $LOGINUSR "trizen -S --noconfirm numix-circle-icon-theme-git"
rm -Rf /usr/share/icons/Numix-Circle-Light/
trizen -Q gtk-theme-windows10-dark || su -c - $LOGINUSR "trizen -S --noconfirm gtk-theme-windows10-dark"

# the hell of qtwebkit (required for SP flashtool ONLY)
# Even as a fallback this takes HOURS!
# as building for 32bit was not possible I use the precompiled i686 package from:
# https://sourceforge.net/projects/bluestarlinux/files/repo/i686/
TGZWK=/home/$LOGINUSR/.fwul/tmp/qtwebkit-*-${arch}.pkg.tar.xz
pacman -Q qtwebkit || pacman --noconfirm -U $TGZWK

# Create a MD5 for a given file or set to 0 if file is missing
F_DOMD5(){
    MFILE="$1"
    [ -z "$MFILE" ]&& echo "MISSING ARG FOR $FUNCNAME!" && exit 3
    if [ -f "$MFILE" ];then
        md5sum $MFILE | cut -d " " -f 1
    else
        echo 0
    fi
}

# wait until a file gets written or modified
F_FILEWAIT(){
    MD5C=$1
    FILE="$2"

    [ -z "$FILE" -o -z "$MD5C" ] && echo "MISSING ARG FOR $FUNCNAME!" && exit 3
    while true; do
        MD5NOW=$(F_DOMD5 "$FILE")
        if [ "$MD5C" != "$MD5NOW" ];then
            break
        else
            echo -e "\t.. waiting that the changes gets written for $FILE.."
            sleep 1s
        fi
    done
}

# ensure proper perms (have to be done BEFORE using any su LOGINUSER cmd which writes to home dir!)
chown -R ${LOGINUSR}.users /home/$LOGINUSR/

# activate wallpaper, icons & theme
echo -e "\nActivate theme etc:"
FWULDESKTOP="/home/$LOGINUSR/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml"
FWULXFWM4="/home/$LOGINUSR/.config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml"
FWULXSETS="/home/$LOGINUSR/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml"

if [ ! -f "$FWULDESKTOP" ];then
    echo -e "\t... setting desktop wallpaper"
    MD5BEF=$(F_DOMD5 "$FWULDESKTOP")
    su -c - $LOGINUSR "dbus-launch xfconf-query --create -t string -c xfce4-desktop -s /home/$LOGINUSR/.fwul/wallpaper_fwul.png -p /backdrop/screen0/monitor0/workspace0/last-image"
    F_FILEWAIT $MD5BEF "$FWULDESKTOP"
    # stretch wallpaper
    MD5BEF=$(F_DOMD5 "$FWULDESKTOP")
    su -c - $LOGINUSR "dbus-launch xfconf-query --create -t int -c xfce4-desktop -s 3 -p /backdrop/screen0/monitor0/workspace0/image-style"
    F_FILEWAIT $MD5BEF "$FWULDESKTOP"
fi
if [ ! -f "$FWULXFWM4" ];then
    echo -e "\t... setting themes and icons 1"
    MD5BEF=$(F_DOMD5 "$FWULXFWM4")
    su -c - $LOGINUSR "dbus-launch xfconf-query --create -t string -c xfwm4 -p /general/theme -s Windows10Dark"
    F_FILEWAIT $MD5BEF "$FWULXFWM4"
fi
if [ ! -f "$FWULXSETS" ];then
    echo -e "\t... setting themes and icons 2"
    MD5BEF=$(F_DOMD5 "$FWULXSETS")
    su -c - $LOGINUSR "dbus-launch xfconf-query --create -t string -c xsettings -p /Net/ThemeName -s Windows10Dark"
    F_FILEWAIT $MD5BEF "$FWULXSETS"
    MD5BEF=$(F_DOMD5 "$FWULXSETS")
    su -c - $LOGINUSR "dbus-launch xfconf-query --create -t string -c xsettings -p /Net/IconThemeName -s Numix-Circle"
    F_FILEWAIT $MD5BEF "$FWULXSETS"
fi

# weird issue with text shadows on icons
MD5BEF=$(F_DOMD5 "$FWULDESKTOP")
su -c - $LOGINUSR "dbus-launch xfconf-query -c xfce4-desktop -p /desktop-icons/center-text -n -t bool -s false"
F_FILEWAIT $MD5BEF "$FWULDESKTOP"

# set icon text color & transparency for XFCE desktop (theme based!)
# usually it should be enough to do the change in .gtkrc-2.0 only but without this hack it won't be included in the Win10dark theme:
grep -v "fg\[NORMAL\] = \"#ffffff\"" /usr/share/themes/Windows10Dark/gtk-2.0/Apps/xfce.rc > /usr/share/themes/Windows10Dark/gtk-2.0/Apps/xfce.rc.new
mv /usr/share/themes/Windows10Dark/gtk-2.0/Apps/xfce.rc.new /usr/share/themes/Windows10Dark/gtk-2.0/Apps/xfce.rc
grep -v "fg\[NORMAL\] = @selected_fg_color" /usr/share/themes/Windows10Dark/gtk-2.0/gtkrc > /usr/share/themes/Windows10Dark/gtk-2.0/gtkrc.new
mv /usr/share/themes/Windows10Dark/gtk-2.0/gtkrc.new /usr/share/themes/Windows10Dark/gtk-2.0/gtkrc
grep -v "XfdesktopIconView::selected-label-alpha" /usr/share/themes/Windows10Dark/gtk-2.0/gtkrc > /usr/share/themes/Windows10Dark/gtk-2.0/gtkrc.new
mv /usr/share/themes/Windows10Dark/gtk-2.0/gtkrc.new /usr/share/themes/Windows10Dark/gtk-2.0/gtkrc

# set aliases
echo -e '\n# FWUL aliases\nalias fastboot="sudo fastboot"\n' >> /home/$LOGINUSR/.bashrc

###############################################################################################################
#
# temporary workarounds
#
###############################################################################################################

# vbox utils bug
# https://code.binbash.it:8443/FWUL/build_fwul/issues/93
vbinf=$(pacman -Q virtualbox-guest-utils | cut -d " " -f2)
if [ "$vbinf" == "6.0.0-3" ];then
    pacman --noconfirm -Rn virtualbox-guest-utils linux419-virtualbox-guest-modules
    pacman --noconfirm -S linux419-headers
    wget -O /tmp/vbox-utils.tar "http://leech.binbash.it:8008/FWUL/.repo/vbox-utils.tar"
    cd /tmp && tar xvf vbox-utils.tar
    pacman --noconfirm -U /tmp/virtualbox-guest-*
    rm /tmp/virtualbox-guest-*
    systemctl enable vboxservice
    # ensure we do not remove the module when removing kernel headers
    rm -vf /usr/src/vboxguest*/dkms.conf
    pacman --noconfirm -Rn linux419-headers
fi

###############################################################################################################
#
# cleanup
#
###############################################################################################################
echo -e "\nCleanup - locale & MAN pages:"
trizen -Q localepurge || su -c - $LOGINUSR "trizen -S --noconfirm localepurge"
# set the locales we want to keep:
cut -d ' ' -f1 /etc/locale.gen >> /etc/locale.nopurge
cut -d ' ' -f1 /etc/locale.gen | cut -d '.' -f 1 >> /etc/locale.nopurge
cut -d ' ' -f1 /etc/locale.gen | cut -d '.' -f 1 | cut -d "_" -f1 >> /etc/locale.nopurge

# purge locales and manpages:
localepurge -v
for localeinuse in $(find /usr/share/locale/ -maxdepth 1 -type d |cut -d "/" -f5 );do 
    grep -q $localeinuse /etc/locale.gen || rm -rfv /usr/share/locale/$localeinuse
done

echo -e "\nCleanup - pacman:"
IGNPKG="adwaita-icon-theme lvm2 man-db man-pages mdadm nano netctl openresolv pcmciautils reiserfsprogs s-nail vi xfsprogs zsh memtest86+ caribou gnome-backgrounds gnome-themes-extra gnome-themes-standard nemo telepathy-glib zeitgeist gnome-icon-theme webkit2gtk progsreiserfs linux316 linux316-virtualbox-guest-modules qt5-tools btrfs-progs ruby rubygems"
for igpkg in $IGNPKG;do
    PFOUND=1
    pacman -Q $igpkg || PFOUND=0 2>&1 >> /dev/null
    [ $PFOUND -eq 1 ] && echo "trying to remove $igpkg as it seems to be installed.." && pacman --noconfirm -Rns -dd $igpkg
    [ $PFOUND -eq 0 ] && echo "PACMAN: package $igpkg not found for removal"
    true
done

echo -e "\nCleanup - python stuff:"
for pydir in "/usr/lib/python2.*" "/usr/lib/python3.*";do
    echo "deleting test dir for $pydir"
    rm -rvf $pydir/test/*
    echo "deleting pyo's,pyc's & pycaches in $pydir"
    find "$pydir" -type f -name "*.py[co]" -delete -print || echo "nothing to clean in $pydir"
    find "$pydir" -type d -name "__pycache__" -delete -print 2>/dev/null || echo "nothing to clean in $pydir"
done

echo -e "\nCleanup - folders:"
DELFOLD="/usr/share/icons/HighContrast /usr/share/icons/hicolor /usr/share/icons/locolor /share/icons/Adwaita /usr/share/icons/gnome /usr/share/icons/Numix-Light /usr/share/icons/gnome"
for delf in $DELFOLD;do
    [ -d "$delf" ] && rm -vrf "$delf"
    true
done

echo -e "\nCleanup - pacman orphans:"
PMERR=$(pacman --noconfirm -Rns $(pacman -Qtdq) || echo no pacman orphans)
echo -e "\nCleanup - trizen orphans:"
YERR=$(su -c - $LOGINUSR "trizen -Qtd --noconfirm" || echo no trizen orphans)

echo -e "\nCleanup - manpages:"
rm -rvf /usr/share/man/*

echo -e "\nCleanup - docs:"
rm -rvf /usr/share/doc/* /usr/share/gtk-doc/html/*

echo -e "\nCleanup - misc:"
rm -rvf /*.tgz /*.tar.gz /trizen/ /package-query/ /home/$LOGINUSR/.fwul/tmp/*

echo -e "\nCleanup - arch dependent:"
if [ $arch == "i686" ];then
    rm "/home/$LOGINUSR/.fwul/jre-8u131-1-x86_64.pkg.tar.xz"
else
    rm "/home/$LOGINUSR/.fwul/jre-8u131-1-i686.pkg.tar.xz"
fi

echo -e "\nCleanup - archiso:"
rm -rvf /etc/fwul

echo -e "\nCleanup - linux firmware"
rm -rvf /usr/lib/firmware/netronome /usr/lib/firmware/liquidio

# persistent perms for fwul
cat > $RSUDOERS <<EOSUDOERS
%wheel     ALL=(ALL) ALL

# special rules for session & language
%wheel     ALL=(ALL) NOPASSWD: /bin/mount -o remount\,size=* /run/archiso/cowspace
%wheel     ALL=(ALL) NOPASSWD: /bin/umount -l /tmp
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /var/tmp/* /tmp/
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /tmp/locale.conf /etc/locale.conf
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /tmp/environment /etc/environment
# needed for language workaround
# https://code.binbash.it:8443/FWUL/build_fwul/issues/88
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/systemctl restart lightdm

# let the user sync the databases without asking for pw
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/trizen --noconfirm -Sy
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman --noconfirm -Sy
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman -Sy --noconfirm

# let the user install deps and compiled packages by trizen without asking for pw
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman --color auto -S --asdeps --needed --noconfirm *
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman --color auto -U --asdeps --noconfirm *
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman --color auto -U --noconfirm /tmp/trizen-tmp-$LOGINUSR/*/*.pkg.tar.xz
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman *.pkg.tar.xz -Ud *
%wheel     ALL=(ALL) NOPASSWD: /bin/cp -vf /tmp/trizen-$LOGINUSR/*/*.pkg.tar.xz /var/cache/pacman/*
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman --noconfirm -S --asdeps*

# when using fwul package installer no pw 
%wheel     ALL=(ALL) NOPASSWD: /home/$LOGINUSR/.fwul/install_package.sh *

# query file sizes without any prompt
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman --print-format %s -S *

# special rules for TeamViewer
%wheel     ALL=(ALL) NOPASSWD: /bin/systemctl start teamviewerd
%wheel     ALL=(ALL) NOPASSWD: /bin/systemctl enable teamviewerd

# special rule for Sony Flashtool
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/trizen --noconfirm -S xperia-flashtool
%wheel     ALL=(ALL) NOPASSWD: /bin/cp x10flasher.jar /usr/lib/xperia-flashtool/
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman --noconfirm -U /tmp/arch_xperia-flashtool/xperia-flashtool*.pkg.tar.xz
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman --noconfirm -U xperia-flashtool*.pkg.tar.xz

# special rule for SP Flashtool
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/trizen --noconfirm -S spflashtool-bin

# special rule for JAVA
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman -U --noconfirm /home/$LOGINUSR/.fwul/$CURJAVA

# special rule for fastboot
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/fastboot *

# SALT
%wheel     ALL=(ALL) NOPASSWD: /home/$LOGINUSR/programs/SALT/salt

# FWUL mode
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /tmp/fwul-release /etc/fwul-release
EOSUDOERS

# set root password
passwd root <<EOSETPWROOTPW
$RPW
$RPW
EOSETPWROOTPW

# set real release info
echo "fwulversion=$iso_version" > /etc/fwul-release
echo "fwulbuild=$(date +%s)" >> /etc/fwul-release
echo "patchlevel=0" >> /etc/fwul-release

# media fix
[ ! -d /media ] && mkdir /media
chmod 755 /media

# etc fix
chown -R root.root /etc

# set default user for lightdm
test -d /var/lib/lightdm/.cache/lightdm-gtk-greeter || mkdir -p /var/lib/lightdm/.cache/lightdm-gtk-greeter/
echo -e '[greeter]\nlast-user=android' > /var/lib/lightdm/.cache/lightdm-gtk-greeter/state && chmod 755 /var/lib/lightdm/.cache/lightdm-gtk-greeter/state

# ensure hosts does not contain build stuff
mv /etc/hosts.orig /etc/hosts

########################################################################################
# TEST AREA - TEST AREA - TEST AREA 

echo -e "\nTESTING FWUL BUILD!"

if [ -e /boot/vmlinuz-linux ];then 
    echo "Kernel in place!"
else
    echo "ERROR: Kernel symlink not valid!"
    ls -la /boot
    false
fi

# arch independent requirements
REQFILES="/home/$LOGINUSR/.fwul/wallpaper_fwul.png 
$FWULDESKTOP
$FWULXFWM4
$FWULXSETS
$RSUDOERS
/home/$LOGINUSR/Desktop/LG/open-lglafshell.desktop
/home/$LOGINUSR/Desktop/LG/open-lglafng.desktop
/home/$LOGINUSR/programs/SALT/salt
/home/$LOGINUSR/programs/lglafng/partitions.py
/home/$LOGINUSR/programs/lglaf/partitions.py
/home/$LOGINUSR/programs/kdztools/unkdz.py
/home/$LOGINUSR/Desktop/LG/SALT.desktop
/usr/share/mdm/themes/Arc-Wise/MdmGreeterTheme.desktop
/home/$LOGINUSR/Desktop/ADB.desktop
/home/$LOGINUSR/programs/sadb/starter.sh
/home/$LOGINUSR/programs/sadb/S-ADB.jar
/home/$LOGINUSR/Desktop/Samsung/heimdall.desktop
/home/$LOGINUSR/Desktop/install-TV.desktop
/usr/bin/adb
/usr/bin/fastboot
/usr/bin/heimdall
/usr/bin/trizen
/usr/bin/qphotorec
/home/$LOGINUSR/Desktop/qphotorec.desktop
/home/$LOGINUSR/.fwul/odin-logo.jpg
/home/$LOGINUSR/.fwul/install_spflash.sh
/home/$LOGINUSR/.fwul/install_sonyflash.sh
/home/$LOGINUSR/Desktop/install-sonyflash.desktop
/usr/share/icons/Numix-Circle/index.theme
/home/$LOGINUSR/.android/adb_usb.ini
/etc/udev/rules.d/51-android.rules
/home/$LOGINUSR/programs/welcome/welcome.sh
/home/$LOGINUSR/programs/welcome/icons/welcome.png
/home/$LOGINUSR/.config/autostart/welcome.desktop
/etc/systemd/scripts/fwul-session.sh
/etc/profile.d/fwul-session.sh
/etc/systemd/system/init-mirror.service
/etc/systemd/scripts/init-fwul
/home/$LOGINUSR/Desktop/welcome.desktop
/etc/fwul-release
/usr/local/bin/livepatcher.sh
/usr/local/bin/liveupdater.sh
/var/lib/fwul/generic.vars
/var/lib/fwul/generic.func
/home/$LOGINUSR/.config/xfce4/desktop/icons.screen0.rc
/home/$LOGINUSR/.fwul/sshkeygen.sh
/home/$LOGINUSR/.config/autostart/sshkeygen.desktop
/home/$LOGINUSR/.fwul/$CURJAVA
/home/$LOGINUSR/.fwul/pkexecgui
/home/$LOGINUSR/Desktop/tmate.desktop
/home/$LOGINUSR/.fwul/tmate.sh
/home/$LOGINUSR/.fwul/tmate-logo.png
/home/$LOGINUSR/.config/autostart/hex.desktop
/home/$LOGINUSR/.fwul/hex.sh
/home/$LOGINUSR/Desktop/io.github.Hexchat.desktop
/home/$LOGINUSR/Desktop/Xiaomi/miflash.desktop
/home/$LOGINUSR/programs/MiFlash/README.md
/home/$LOGINUSR/programs/MiFlash/Xiaomi.png
/home/$LOGINUSR/programs/MiFlash/starter.sh
/home/$LOGINUSR/programs/ROME/rome
/home/$LOGINUSR/Desktop/rome.desktop
/home/$LOGINUSR/.config/autostart/language.desktop
/home/$LOGINUSR/.fwul/checklang.sh
/etc/pacman.ignore"

# 32bit requirements (extend with 32bit ONLY.
# If the test is the same for both arch use REQFILES instead)
REQFILES_i686="$REQFILES"

# 64bit requirements (extend with 64bit ONLY. 
# If the test is the same for both arch use REQFILES instead)
REQFILES_x86_64="$REQFILES
/home/$LOGINUSR/Desktop/install-spflash.desktop
/home/$LOGINUSR/Desktop/Samsung/JOdin.desktop
/home/$LOGINUSR/programs/JOdin/starter.sh
/home/$LOGINUSR/programs/JOdin/JOdin3CASUAL"


CURREQ="REQFILES_${arch}"

for req in $(echo -e "${!CURREQ}"|tr "\n" " ");do
    if [ -f "$req" ];then
        echo -e "\t... testing ($arch): $req --> OK"
    else
        echo -e "\t******************************************************************************"
        echo -e "\tERROR: testing ($arch) $req --> FAILED!!"
        echo -e "\t******************************************************************************"
        exit 3
    fi
done

# add a warning when debugging is enabled
if [ "$DEBUG" -eq 1 ];then
        echo -e "\t******************************************************************************"
        echo -e "\tWARNING: DEBUG MODE ENABLED !!!"
        echo -e "\t******************************************************************************"
fi

# TEST AREA - END
########################################################################################

# list the biggest packages installed
pacman --noconfirm -S expac
expac -H M -s "%-30n %m" | sort -rhk 2 | head -n 40
pacman --noconfirm -Rns expac

# create a XDA copy template for the important FWUL package versions
echo -ne '[*]Versions of the main FWUL components:\n[INDENT]Kernel -> [B]'version:$(pacman -Qi linux419 |grep Version| cut -d ":" -f 2)'[/B]\nADB and fastboot: '
pacman -Q android-tools | sed 's/ / -> [B]version: /g;s/$/[\/B]/g'
echo -e 'simple-adb GUI -> [B]version: XXXXXXXXXXXXX[/B]'
echo -e 'SALT -> [B]version: '$(egrep -o 'VDIG=.*' /home/$LOGINUSR/programs/SALT/salt.vars | cut -d '"' -f2)'[/B]'
echo -e 'ROME -> [B]version: '$(egrep -o 'VDIG=.*' /home/$LOGINUSR/programs/ROME/rome.vars | cut -d '"' -f2)'[/B]'
echo -e 'TeamViewer (req user install) -> [B]version: XXXXXXXXXXXXX[/B]'
CHLOG="bootimgtool-git heimdall-git xfwm4 lightdm xorg-server virtualbox-guest-utils firefox hexchat testdisk-wip tmate"
for i in $CHLOG;do
        pacman -Q $i | sed 's/ / -> [B]version: /g;s/$/[\/B]/g'
done
echo -e '[/INDENT]'

# fix perms
# https://code.binbash.it:8443/FWUL/build_fwul/issues/58
chown -v root.root /usr
chmod -v 755 /usr
chown -v root.root /usr/share
chmod -v 755 /usr/share

#########################################################################################
# this has always to be the very last thing!
rm -vf $TMPSUDOERS
